package com.tg.demoBot;

import jakarta.validation.constraints.NotNull;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class TgUtils {

    private final static int MAX_BUTTONS_PER_ONE_ROW = 3;

    public static SendMessage createMessage(Update update, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(getChatId(update));
        sendMessage.setText(text);
        return sendMessage;
    }

    public static InlineKeyboardButton createInlineButtonWithLink(String btnText, String urlToOpen) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(btnText);
        button.setUrl(urlToOpen);
        return button;
    }

    public static InlineKeyboardButton createInlineButton(String btnText, String callbackData) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(btnText);
        button.setCallbackData(callbackData);
        return button;
    }

    public static long getChatId(Update update) {
        Long chatId = 0L;
        if (update.hasMessage())
            chatId = update.getMessage().getChatId();
        else if (update.hasCallbackQuery())
            chatId = update.getCallbackQuery().getMessage().getChatId();

        return chatId;
    }

    private static InlineKeyboardButton getButton(String btnText, String btnDataOrUrl) {
        if ("https://".contains(btnDataOrUrl) || "http://".contains(btnDataOrUrl))
            return createInlineButtonWithLink(btnText, btnDataOrUrl);
        else
            return createInlineButton(btnText, btnDataOrUrl);
    }

    public static ReplyKeyboard getInlineButtons(@NotNull Map<String, String> data) {
        List<List<InlineKeyboardButton>> allButtons = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        for (var item : data.entrySet()) {
            row.add(getButton(item.getKey(), item.getValue()));

            if (row.size() == MAX_BUTTONS_PER_ONE_ROW) {
                allButtons.add(row.stream().toList());
                row.clear();
            }
        }
        if (row.size() > 0) {
            allButtons.add(row);
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(allButtons);
        return keyboardMarkup;
    }
}