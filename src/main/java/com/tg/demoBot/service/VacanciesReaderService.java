package com.tg.demoBot.service;

import com.tg.demoBot.dto.VacancyDto;

import java.util.List;

public interface VacanciesReaderService {
    List<VacancyDto> getVacancies();
}