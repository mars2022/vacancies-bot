package com.tg.demoBot.service;

import com.tg.demoBot.dto.VacancyDto;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class VacancyServiceImpl implements VacancyService {
    private final Map<Integer, VacancyDto> vacancies = new HashMap<>();
    private VacanciesReaderService readerService;

    @Autowired
    public VacancyServiceImpl(VacanciesReaderService readerService) {
        this.readerService = readerService;
    }

    @PostConstruct
    public void init() {
        List<VacancyDto> vacanciesList = readerService.getVacancies();
        for (VacancyDto vacancy : vacanciesList) {
            vacancies.put(vacancy.getId(), vacancy);
        }
        vacanciesList = null;
    }

    @Override
    public List<VacancyDto> getVacancies(String jobLevel) {
        var targetJob = jobLevel.toLowerCase();
        return vacancies.values().stream()
                .filter(job -> job.getTitle().toLowerCase().contains(targetJob))
                .toList();
    }

    @Override
    public Optional<VacancyDto> getVacancy(int id) {
        return Optional.ofNullable(vacancies.get(id));
//        return Optional.ofNullable(vacancies.getOrDefault(id, null));
    }
}