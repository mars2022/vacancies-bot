package com.tg.demoBot.service;

import com.tg.demoBot.dto.VacancyDto;

import java.util.List;
import java.util.Optional;

public interface VacancyService {
    List<VacancyDto> getVacancies(String jobLevel);
    Optional<VacancyDto> getVacancy(int id);
}