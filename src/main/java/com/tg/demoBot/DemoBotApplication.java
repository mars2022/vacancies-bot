package com.tg.demoBot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBotApplication.class, args);
		System.out.println("app started");
	}
}
