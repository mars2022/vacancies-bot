package com.tg.demoBot;

import com.tg.demoBot.dto.VacancyDto;
import com.tg.demoBot.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class VacanciesBot extends TelegramLongPollingBot {
    @Value("${bot.name}")
    private String botName;

    @Value("${max.buttons.per.one.row}")
    private int MAX_VACANCIES_PER_ONE_ROW;

    private static final String JOB_LEVEL_ID_PREFIX = "levelId=";
    private static final String VACANCY_ID_PREFIX = "vacancyId=";
    private static final String BACK_TO_START_MENU_CB_DATA = "backToStartMenu";

    private final VacancyService vacancyService;
    private final List<String> jobLevels = List.of("Junior", "Middle", "Senior");

    private final Map<Long, Integer> lastShownJobLevel = new HashMap<>();

    @Autowired
    public VacanciesBot(@Value("${bot.token}") String botToken, VacancyService service) {
        super(botToken);
        vacancyService = service;
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public void onUpdateReceived(Update update) {
//        System.out.println(update);
        SendMessage sendMessage;
        if (update.getMessage() != null) {

            sendMessage = TgUtils.createMessage(update, "Welcome to vacancies bot, select your title:");
            sendMessage.setReplyMarkup(getStartMenu());

            send(sendMessage);

        } else if (update.hasCallbackQuery()) {
            String callbackQueryData = update.getCallbackQuery().getData();
            //System.out.println(callbackQueryData);

            if (callbackQueryData.startsWith(JOB_LEVEL_ID_PREFIX)) {//show jobs buttons

                int jobLevelId = parseId(callbackQueryData);
                lastShownJobLevel.put(TgUtils.getChatId(update), jobLevelId);

                sendMessage = TgUtils.createMessage(
                        update, "Please, select your %s vacancy:".formatted(jobLevels.get(jobLevelId)));
                sendMessage.setReplyMarkup(getVacanciesMenuButtons(jobLevelId));

                send(sendMessage);

            } else if (callbackQueryData.startsWith(VACANCY_ID_PREFIX)) {//show one vacancy Description

                processOneVacancy(update);

            } else if (callbackQueryData.startsWith(BACK_TO_START_MENU_CB_DATA)) {
                sendMessage = TgUtils.createMessage(update, "Please, select your title:");
                sendMessage.setReplyMarkup(getStartMenu());

                send(sendMessage);
            }
        }
    }

    private void send(SendMessage sendMessage) {
        try {
            execute(sendMessage);//createMessage(update, "you sent: "+update.getMessage().getText())
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }

    private ReplyKeyboard getStartMenu() {
        List<InlineKeyboardButton> row = new ArrayList<>();
        for (int i = 0; i < jobLevels.size(); i++) {
            row.add(TgUtils.createInlineButton(jobLevels.get(i), mapToCallbackDataLevelId(i)));
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(List.of(row));
        return keyboardMarkup;
    }

    private ReplyKeyboard getBackButtonsForVacancy(int levelId, boolean showOpenaiBtn) {
        if (levelId < 0)
            throw new IllegalArgumentException();

        List<InlineKeyboardButton> row = new ArrayList<>();
        row.add(TgUtils.createInlineButton("Back to vacancies", mapToCallbackDataLevelId(levelId)));
        row.add(TgUtils.createInlineButton("Back to start menu", BACK_TO_START_MENU_CB_DATA));
        if (showOpenaiBtn) {
            row.add(TgUtils.createInlineButtonWithLink(
                    "Get cover letter", "https://chat.openai.com/"));
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(List.of(row));
        return keyboardMarkup;
    }

    private void processOneVacancy(Update update) {
        if (!update.hasCallbackQuery()) return;

        int jobId = parseId(update.getCallbackQuery().getData());
        int jobLevelId = lastShownJobLevel.getOrDefault(TgUtils.getChatId(update), -1);

        vacancyService.getVacancy(jobId)
            .ifPresentOrElse(vacancy -> {
                    SendMessage sendMessage = TgUtils.createMessage(update, formatVacancy(vacancy));
                    sendMessage.setParseMode(ParseMode.MARKDOWN);

                    sendMessage.setReplyMarkup(getBackButtonsForVacancy(jobLevelId, true));
                    send(sendMessage);
                }, () -> {
                    SendMessage sendMessage = TgUtils.createMessage(
                            update, "An error happened or vacancy is expired");

                    sendMessage.setReplyMarkup(getBackButtonsForVacancy(jobLevelId, false));
                    send(sendMessage);
                }
            );
    }

    private String formatVacancy(VacancyDto job) {
        return """
            *%s*
            *Company:* %s
            *Short description:* %s
            \n*Description:* %s
            *Salary:* %s
            *Link:* [%s] (%s)
            """.formatted(
                escapeMarkdownReservedChars(job.getTitle()),
                escapeMarkdownReservedChars(job.getCompany()),
                escapeMarkdownReservedChars(job.getShortDescription()),
                escapeMarkdownReservedChars(job.getDescription()),
                job.getSalary().isBlank() || job.getSalary().length() < 2
                    ? "Not specified"
                    : escapeMarkdownReservedChars(job.getSalary()),
                "Click here for more details",
                escapeMarkdownReservedChars(job.getLink())
        );
    }

    private String escapeMarkdownReservedChars(String input) {/*
        System.out.println(input);
        input = input.replace("-", "\\-")
            .replace("_", "\\_")
            .replace("*", "\\*")
            .replace("[", "\\[")
            .replace("]", "\\]")
            .replaceAll("\\(", "\\\\(")
//            .replace("(", "\\(")
            .replace(")", "\\)")
            .replace("~", "\\~")
            .replace("`", "\\`")
//            .replace("<", "\\<")
            .replace(">", "\\>")
            .replace("#", "\\#")
            .replace("+", "\\+")
            .replace(".", "\\.")
            .replace("!", "\\!");
        System.out.println(input);*/
        return input;
    }

    private ReplyKeyboard getVacanciesMenuButtons(int levelId) {
        if (levelId < 0)
            throw new IllegalArgumentException();

        List<VacancyDto> vacancies = vacancyService.getVacancies(jobLevels.get(levelId));

        List<List<InlineKeyboardButton>> allButtons = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        for (VacancyDto vacancy : vacancies) {
            row.add(TgUtils.createInlineButton(
                    vacancy.getTitle(),
                    mapToCallbackDataVacancyId(vacancy.getId())));
            if (row.size() == MAX_VACANCIES_PER_ONE_ROW) {
                allButtons.add(row.stream().toList());
                row.clear();
            }
        }
        if (row.size() > 0) {
            allButtons.add(row);
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(allButtons);
        return keyboardMarkup;
    }

    private static String mapToCallbackDataLevelId(int id) {
        return JOB_LEVEL_ID_PREFIX + id;
    }

    private static String mapToCallbackDataVacancyId(int id) {
        return VACANCY_ID_PREFIX + id;
    }

    private int parseId(String callbackData) {
        String[] strings = callbackData.split("=");
        if (strings.length == 2) {
            try {
                return Integer.parseInt(strings[1]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }
/*
    private SendMessage createMessage(Update update, String text) {
//        Long chatId;
//        if (update.hasMessage())
//            chatId = update.getMessage().getChatId();
//        else
//            chatId = update.getCallbackQuery().getMessage().getChatId();

    SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(TgUtils.getChatId(update));
        sendMessage.setText(text);
        return sendMessage;
}

    private InlineKeyboardButton createInlineButtonWithLink(String text, String urlToOpen) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setUrl(urlToOpen);
        return button;
    }

    private InlineKeyboardButton createInlineButton(String text, String callbackData) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(callbackData);
        return button;
    }

    private static long getChatId(Update update) {
        Long chatId = 0L;
        if (update.hasMessage())
            chatId = update.getMessage().getChatId();
        else if (update.hasCallbackQuery())
            chatId = update.getCallbackQuery().getMessage().getChatId();

        return chatId;
    }

    private ReplyKeyboard getVacanciesMenuButtons(int levelId) {
        if (levelId < 0)
            throw new IllegalArgumentException();

        List<VacancyDto> vacancies = vacancyService.getVacancies(jobLevels.get(levelId));
        List<InlineKeyboardButton> row = new ArrayList<>();
        for (VacancyDto vacancy : vacancies) {
            row.add(createInlineButton(
                    vacancy.getTitle(), mapToCallbackDataVacancyId(vacancy.getId())));
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(List.of(row));
        return keyboardMarkup;
    }

    private ReplyKeyboard getVacanciesMenu(int levelId) {
        if (levelId < 0)
            throw new IllegalArgumentException();

        final int[][] jobIds = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8}};

        int[] jobForCurrentLevelIds = jobIds[levelId];

        List<InlineKeyboardButton> row = new ArrayList<>();
        for (int jobId : jobForCurrentLevelIds) {
            row.add(createInlineButton(
                    jobLevels.get(levelId) + "'s vacancy #" + jobId,
                    mapToVacancyId(jobId)));
        }

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(List.of(row));
        return keyboardMarkup;
    }*/
}